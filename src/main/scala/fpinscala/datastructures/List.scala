package fpinscala.datastructures

sealed trait List[+A] {

}
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x,xs) => x + sum(xs)
  }
  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x,xs) => x * product(xs)
  }
  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  // 3.2
  def tail[A](l: List[A]): List[A] = l match {
    case Nil => sys.error("Tail of empty list")
    case Cons(_, tail) => tail
  }

  // Exercise 3.3
  def setHead[A](l:List[A], head:A): List[A] = l match {
    case Nil => sys.error("Setting head of empty list")
    case Cons(_, tail) => Cons(head, tail)
  }

  //Exercise 3.4
  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Nil => Nil
    case Cons(_, tail) if n >0 => drop(tail, n-1)
    case _ => l
  }

  // Exercise 3.5
//  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
//    case Nil => Nil
//    case Cons(head, tail) => if(f(head)) dropWhile(tail, f) else Cons(head, tail)
//  }
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Cons(head, tail) if f(head) => dropWhile(tail, f)
    case _ => l
  }

  def append[A](a1: List[A], a2: List[A]): List[A] =
    a1 match {
      case Nil => a2
      case Cons(h,t) => Cons(h, append(t, a2))
    }

  // !! Exercise 3.6
  def init[A](l: List[A]): List[A] =
    l match {
      case Nil => sys.error("init of empty list")
      case Cons(_,Nil) => Nil
      case Cons(h,t) => Cons(h,init(t))
    }
  def init2[A](l: List[A]): List[A] = {
    import collection.mutable.ListBuffer
    val buf = new ListBuffer[A]
    @annotation.tailrec
    def go(cur: List[A]): List[A] = cur match {
      case Nil => sys.error("init of empty list")
      case Cons(_,Nil) => List(buf.toList: _*)
      case Cons(h,t) => buf += h; go(t)
    }
    go(l)
  }

  def foldRight_1[A,B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(h, t) => f(h, foldRight_1(t, z)(f))
  }

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def foldLeft_1[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => foldLeft_1(xs, f(z, x))(f)
  }


  def sum2(ns: List[Int]) =
    foldRight(ns, 0)((x,y) => x + y)
  def product2(ns: List[Double]) =
    foldRight(ns, 1.0)(_ * _)

  val fr:((Int, Int) => Int) => Int = foldRight(List(1,2,3), 0)

  // Exerecise 3.9
  def length[A](as: List[A]): Int = foldRight(as, 0)((_,y) => 1 + y)

  // Exerecise 3.10
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(h, t) => foldLeft(t, f(z, h))(f)
  }

  def map[A,B](as: List[A], f: A=>B): List[B] = foldRight(as, Nil: List[B])((h, t) => Cons(f(h), t))

  def zipWith[A,B,C](l1: List[A], l2: List[B])(f: (A,B) => C): List[C] = (l1, l2) match {
    case (Cons(h1,t1), Cons(h2,t2)) => Cons(f(h1,h2),zipWith(t1, t2)(f))
    case _ => Nil
  }
}

object Test extends App {
  val l = Cons(1, Cons(2, Cons(3, Nil)))

  println(List.drop(l, 2))

  val a: List[Int] = Nil
}