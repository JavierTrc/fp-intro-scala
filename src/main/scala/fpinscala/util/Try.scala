package fpinscala.util

sealed trait Try[+T]
case class Success[+T](value: T) extends Try[T]
case class Failure[+T](error: Throwable) extends Try[T]