package fpinscala.state

sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int) {

  def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = {
    inputs.foldLeft(State.get[Machine]){ (state, input) =>
      state.flatMap(_ => State.modify[Machine](machine =>
        input match {
          case Coin if machine.locked && candies > 0 => machine.copy(locked = false, coins = machine.coins + 1)
          case Turn if !machine.locked => machine.copy(locked = true, candies = machine.candies-1)
          case _ => machine
        }
      )).flatMap(_ => State.get)
    }.map(m => (m.coins, m.candies))
  }
}

// Book answer
object Candy {
  import State._
  private def update = (i: Input) => (s: Machine) =>
    (i, s) match {
      case (_, Machine(_, 0, _)) => s
      case (Coin, Machine(false, _, _)) => s
      case (Turn, Machine(true, _, _)) => s
      case (Coin, Machine(true, candy, coin)) =>
        Machine(locked = false, candy, coin + 1)
      case (Turn, Machine(false, candy, coin)) =>
        Machine(locked = true, candy - 1, coin)
    }

  def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = for {
    _ <- sequence(inputs map (modify[Machine] _ compose update))
    s <- get
  } yield (s.coins, s.candies)
}


object CandyMachine extends App {

  val m = Machine(locked = false, 1, 0)

  val i1 = List(Coin, Turn)
  val i2 = List(Turn, Turn, Turn)
  val i3 = List()
  val i4 = List(Coin, Turn, Coin, Turn)


  println("Purchase correctly " + m.simulateMachine(i1).run(m))
  println("Doesn't change " + m.simulateMachine(i1).run(m))
  println("Empty input " + m.simulateMachine(i1).run(m))
  println("Purchase more than once " + m.simulateMachine(i1).run(m))
}