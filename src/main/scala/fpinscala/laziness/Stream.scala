package fpinscala.laziness

sealed trait Stream[+A] {
  import Stream._

  def headOption: Option[A] = this match {
    case Empty => None
    case Cons(h, _) => Some(h())
  }

  // Exercise 5.1
  def toList: List[A] = {
    import collection.mutable.ListBuffer
    val buffer = ListBuffer[A]()

    def go(s: Stream[A]): List[A] = s match {
      case Empty => buffer.toList
      case Cons(h, t) => buffer += h(); go(t())
    }
    go(this)
  }
  // Exercise 5.2
  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 0 => cons(h(), t().take(n-1))
    case Cons(h, _) if n == 1 => cons(h(), empty)
    case _ => empty
  }

  final def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 => t().drop(n - 1)
    case _ => this
  }

  // exercise 5.3
  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h())=> cons[A](h(), t() takeWhile p)
    case _ => empty
  }

  def exists(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exists(p)
    case _ => false
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B =
    this match {
      case Cons(h,t) => f(h(), t().foldRight(z)(f))
      case _ => z
    }

  def foldLeft[B](z: => B)(f:(B, A) => B): B = this match {
    case Cons(h, t) => t().foldLeft(f(z, h()))(f)
    case _ => z
  }

  //Exercise 5.4
  def forAll(p: A => Boolean): Boolean = foldRight(true)((a,b) => p(a) && b)

  //Exercise 5.5
  def takeWhile_1(p: A => Boolean): Stream[A] = foldRight(empty[A])((h, t) =>
    if(p(h)) cons(h, t)
    else empty
  )

  // Exercise 5.6
  def headOption_1(): Option[A] = foldRight(None: Option[A])((h,_) => Some(h))

  // Exercise 5.7
  def map[B](f: A => B): Stream[B] = foldRight(empty[B])((h, acc) => cons(f(h), acc))
  def filter(f: A => Boolean): Stream[A] = foldRight(empty[A])((h,acc) => if(f(h)) cons(h, acc) else acc)
  def append[B>:A](b: => Stream[B]): Stream[B] = foldRight(b)((h, tail) => cons(h, tail))
  def flatMap[B](f: A => Stream[B]): Stream[B] = foldRight(empty[B])((h, t) => f(h) append t)

  def find(p: A => Boolean): Option[A] =
    filter(p).headOption

  // Exercise 5.13
  def _map[B](f: A => B): Stream[B] = unfold(this) {
    case Empty => None
    case Cons(h, t) => Some((f(h()),t()))
  }
  def takeWhile_2(p: A => Boolean): Stream[A] = unfold(this) {
    case Cons(h, t) if p(h()) =>  Some((h(), t()))
    case _ => None
  }

  def zipWith[B,C](s2: Stream[B])(f: (A,B) => C): Stream[C] = unfold((this, s2)) {
    case (Cons(h, t), Cons(h2, t2)) => Some( f(h(),h2()), (t(),t2()) )
    case _ => None
  }
  def zip[B](s2: Stream[B]): Stream[(A,B)] = zipWith(s2){ (_,_) }

  def zipAll[B](s2: Stream[B]): Stream[(Option[A],Option[B])] = unfold((this,s2)) {
    case (Cons(h, t), Cons(h2, t2)) => Some( (Some(h()),Some(h2())), (t(),t2()) )
    case (Empty, Cons(h, t)) => Some(( (None, Some(h())), (Empty, t()) ))
    case (Cons(h, t), Empty) => Some(((Some(h()), None), (t(),Empty)))
    case _ => None
  }

  // Exercise 5.14
  def startsWith[B>:A](s: Stream[B]): Boolean = zipAll(s).takeWhile(_._2.isDefined) forAll {
    case (h1,h2) => h1 == h2
  }
  // Exercise 5.15
  def tails: Stream[Stream[A]] = unfold(this) {
    case s@Cons(_, t) => Some((s, t()))
    case _ => None
  }
  def tails_ans: Stream[Stream[A]] = unfold(this) {
    case Empty => None
    case s => Some((s, s drop 1))
  } append Stream(empty)

  def hasSubsequence[A](s: Stream[A]): Boolean =
    tails exists (_ startsWith s)

  /*
The function can't be implemented using `unfold`, since `unfold` generates elements of the `Stream` from left to right. It can be implemented using `foldRight` though.
The implementation is just a `foldRight` that keeps the accumulated value and the stream of intermediate results, which we `cons` onto during each iteration. When writing folds, it's common to have more state in the fold than is needed to compute the result. Here, we simply extract the accumulated list once finished.
*/
  def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] =
    foldRight((z, Stream(z)))((a, p0) => {
      // p0 is passed by-name and used in by-name args in f and cons. So use lazy val to ensure only one evaluation...
      lazy val p1 = p0
      val b2 = f(a, p1._1)
      (b2, cons(b2, p1._2))
    })._2
}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }
  def empty[A]: Stream[A] = Empty
  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = Stream.cons(1, ones)
  // Exercise 5.8-10
  def constant[A](a: A): Stream[A] = cons(a, constant(a))
  def from(n: Int): Stream[Int] = cons(n, from(n+1))
  def fib(): Stream[Int] = {
    def go(prev: Int, act: Int): Stream[Int] = cons(prev, go(act, prev+act))
    go(0,1)
  }
  // Exercise 5.11
  def unfold[A,S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case None => empty
    case Some((a, s)) => cons(a, unfold(s)(f))
  }
  // Exercise 5.12
  def fibs(): Stream[Int] = unfold(Tuple2(0,1)){ case (prev, act) => Some(prev, (act, prev + act))}
  def from_1(n: Int): Stream[Int] = unfold(n)(x => Some((x, x+1)))
  def constant_1[A](a: A): Stream[A] = unfold(a)(c => Some((c,c)))
  def ones_1(): Stream[Int] = unfold(1)(c => Some((c,c)))
}

object Test extends App {
  val s = Stream(1,2,3,4,5)
  val e = Stream()
  println(s.headOption_1())
  println(e.headOption_1())
}
