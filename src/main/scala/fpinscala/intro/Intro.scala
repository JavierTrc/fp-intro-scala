package fpinscala.intro

import fpinscala.intro.Intro.whileFun

object Intro {

  def abs(n: Int): Int =
    if (n < 0) -n
    else n

  def whileFun(a: Int, p: (Int) => Boolean): Unit = {
    def go(n: Int): Unit = {
      if(p(n)) {println(n); go(n-1) }
      else ()
    }
    go(a)
  }

  def uglyFib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => uglyFib(n - 1) + uglyFib(n - 2)
  }

  // Tail rec example
  def factorial(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, acc: Int): Int =
      if (n <= 0) acc
      else go(n - 1, n * acc)

    go(n, 1)
  }


  // How it actually should look
  def fib(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, prev: Int, act: Int): Int = n match {
      case 0 => prev
      case 1 => act
      case _ => go(n - 1, act, prev + act)
    }

    go(n, 0, 1)
  }


  def formatAbs(x: Int): String = {
    val msg = "The absolute value of %d is %d"
    msg.format(x, abs(x))
  }

  def formatFactorial(n: Int): String = {
    val msg = "The factorial of %d is %d."
    msg.format(n, factorial(n))
  }

  def formatResult(name: String, n: Int, f: Int => Int): String = {
    val msg = "The %s of %d is %d."
    msg.format(name, n, f(n))
  }

//  def findFirst(ss: Array[String], key: String): Int = {
//    @annotation.tailrec
//    def loop(n: Int): Int =
//      if (n >= ss.length) -1
//      else if (ss(n) == key) n
//      else loop(n + 1)
//    loop(0)
//  }

  // Polymorphic functions
  def findFirst[A](as: Array[A], p: A => Boolean): Int = {
    @annotation.tailrec
    def loop(n: Int): Int =
      if (n >= as.length) -1
      else if (p(as(n))) n
      else loop(n + 1)
    loop(0)
  }

  findFirst(Array(1,2,3,4,5), (x: Int) => x%2==0)

  def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = {
    def loop(n: Int): Boolean =
      if (n > as.length - 1) true
      else if(!ordered(as(n), as(n+1))) false
      else loop(n+1)
    loop(0)
  }



  def partial1[A,B,C](a: A, f: (A,B) => C): B => C = (b: B) => f(a,b)

  def curry[A,B,C](f: (A, B) => C): A => B => C = (a: A) => (b: B) => f(a,b)

  def uncurry[A,B,C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a)(b)

  def compose[A,B,C](f: B => C, g: A => B): A => C = (a: A) => f(g(a))
}

object main extends App {

  whileFun(5, _ > 0)
  println(Intro.fib(1))

  val as = Array(1,2,3,4)
  val isSorted = Intro.isSorted(as, (x: Int,y: Int) => x < y)
  print(isSorted)
}
