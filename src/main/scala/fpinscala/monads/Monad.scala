package fpinscala.monads

import fpinscala.applicative.{Applicative, Traverse}
import fpinscala.parallelism.Nonblocking.Par
import fpinscala.parsing.instances.Sliceable
import fpinscala.parsing.instances.SliceableTypes.Parser
import fpinscala.testing.exhaustive.Gen
import fpinscala.laziness.Stream
import fpinscala.state.State

import scala.language.{higherKinds, reflectiveCalls}

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]

  def distribute[A, B](fab: F[(A, B)]): (F[A], F[B]) = (map(fab)(_._1), map(fab)(_._2))

  def codistribute[A, B](e: Either[F[A], F[B]]): F[Either[A, B]] =
    e match {
      case Left(fa) => map(fa)(Left(_))
      case Right(fb) => map(fb)(Right(_))
    }
}

trait Monad[F[_]] extends Applicative[F] { self =>
  def unit[A](a: => A): F[A]

  def flatMap[A, B](ma: F[A])(f: A => F[B]): F[B] = join(map(ma)(f))

  override def map[A, B](ma: F[A])(f: A => B): F[B] = flatMap(ma)(a => unit(f(a)))

  override def map2[A, B, C](ma: F[A], mb: F[B])(f: (A, B) => C): F[C] = flatMap(ma)(a => map(mb)(b => f(a, b)))

  // Exercise 11.3
  override def sequence[A](lma: List[F[A]]): F[List[A]] = lma.foldRight(unit(List[A]()))((fa, acc) => map2(fa, acc)(_ :: _))
  override def traverse[A, B](la: List[A])(f: A => F[B]): F[List[B]] = la.foldRight(unit(List[B]()))((a, acc) => map2(f(a), acc)(_ :: _))

  /**
   * 'Balanced' sequencing, which should behave like `sequence`,
   * but it can use less stack for some data types. We'll see later
   * in this chapter how the monad _laws_ let us conclude both
   * definitions 'mean' the same thing.
   */
  def bsequence[A](ms: Seq[F[A]]): F[IndexedSeq[A]] = {
    if (ms.isEmpty) unit(Vector())
    else if (ms.size == 1) map(ms.head)(Vector(_))
    else {
      val (l, r) = ms.toIndexedSeq.splitAt(ms.length / 2)
      map2(bsequence(l), bsequence(r))(_ ++ _)
    }
  }

  //Exercise 11.4
  // Not tail recursive
  def replicateM_notTR[A](n: Int, ma: F[A]): F[List[A]] = if (n <= 0) unit(List[A]()) else map2(ma, replicateM(n - 1, ma))(_ :: _)

  override def replicateM[A](i: Int, value: F[A]): F[List[A]] = sequence(List.fill(i)(value))

  override def product[A, B](ma: F[A], mb: F[B]): F[(A, B)] = map2(ma, mb)((_, _))

  //Exerice 11.6
  def filterM[A](ms: List[A])(f: A => F[Boolean]): F[List[A]] = ms.foldRight(unit(List[A]()))((a, fas) => flatMap(f(a))(if (_) map(fas)(a :: _) else fas))

  // Exercise 11.7
  def compose[A, B, C](f: A => F[B], g: B => F[C]): A => F[C] = a => flatMap(f(a))(g(_))

  // Exercise 11.8
  def flatMapViaCompose_v1[A, B](ma: F[A])(f: A => F[B]): F[B] = compose[F[A], A, B](fa => fa, a => f(a))(ma)

  def flatMapViaCompose_v2[A, B](ma: F[A])(f: A => F[B]): F[B] = compose((_: Unit) => ma, f)(())

  // Exericse 11.12
  def join[A](mma: F[F[A]]): F[A] = flatMap(mma)(ma => ma)

  // Exercise 11.3
  def flatMapViaJoin[A, B](ma: F[A])(f: A => F[B]): F[B] = join(map(ma)(f))

  def composeViaJoin[A, B, C](f: A => F[B], g: B => F[C]): A => F[C] = a => join(map(f(a))(g))
}

object Monad {
  val genMonad = new Monad[Gen] {
    def unit[A](a: => A): Gen[A] = Gen.unit(a)

    override def flatMap[A, B](ma: Gen[A])(f: A => Gen[B]): Gen[B] =
      ma flatMap f
  }

  // Exercise 11.1
  val parMonad: Monad[Par] = new Monad[Par] {
    override def unit[A](a: => A): Par[A] = Par.unit(a)

    override def flatMap[A, B](ma: Par[A])(f: A => Par[B]): Par[B] = Par.flatMap(ma)(f)
  }

  val parserMonad: Monad[Parser] = new Monad[Parser] {
    override def unit[A](a: => A): Parser[A] = Sliceable.succeed(a)

    override def flatMap[A, B](ma: Parser[A])(f: A => Parser[B]): Parser[B] = Sliceable.flatMap(ma)(f)
  }

  val optionMonad: Monad[Option] = new Monad[Option] {
    override def unit[A](a: => A): Option[A] = Option(a)

    override def flatMap[A, B](ma: Option[A])(f: A => Option[B]): Option[B] = ma flatMap f
  }

  val streamMonad: Monad[Stream] = new Monad[Stream] {
    override def unit[A](a: => A): Stream[A] = Stream(a)

    override def flatMap[A, B](ma: Stream[A])(f: A => Stream[B]): Stream[B] = ma flatMap f
  }

  val listMonad: Monad[List] = new Monad[List] {
    override def unit[A](a: => A): List[A] = List(a)

    override def flatMap[A, B](ma: List[A])(f: A => List[B]): List[B] = ma flatMap f
  }

  // Exercise 11.2
  def stateMonad[S]: Monad[({type T[A] = State[S, A]})#T] = new Monad[({type T[A] = State[S, A]})#T] {
    override def unit[A](a: => A): State[S, A] = State.unit(a)

    override def flatMap[A, B](ma: State[S, A])(f: A => State[S, B]): State[S, B] = ma flatMap f
  }

  // Exercise 11.17 cont
  def idMonad: Monad[Id] = new Monad[Id] {
    override def unit[A](a: => A): Id[A] = Id(a)

    override def flatMap[A, B](ma: Id[A])(f: A => Id[B]): Id[B] = ma flatMap f
  }

  // Exercise 12.5
  def eitherMonad[E]: Monad[({type f[x] = Either[E, x]})#f] = new Monad[({type f[x] = Either[E, x]})#f] {
    override def unit[A](a: => A): Either[E, A] = Right(a)

    override def flatMap[A, B](ma: Either[E, A])(f: A => Either[E, B]): Either[E, B] = ma flatMap f
  }

  def composeM[F[_],G[_]](F: Monad[F], G: Monad[G], T: Traverse[G]): Monad[({type f[x] = F[G[x]]})#f] = new Monad[({type f[x] = F[G[x]]})#f] {
    override def unit[A](a: => A): F[G[A]] = F.unit(G.unit(a))

    override def map[A, B](ma: F[G[A]])(f: A => B): F[G[B]] = F.map(ma)(ga => G.map(ga)(f))

    override def join[A](mma: F[G[F[G[A]]]]): F[G[A]] =
      F.join(F.map[G[F[G[A]]], F[G[A]]](mma)(gfga => F.map[G[G[A]], G[A]](T.sequence[F, G[A]](gfga)(F))(G.join)))
  }
  // Alternative
//  def composeM[G[_],H[_]](implicit G: Monad[G], H: Monad[H], T: Traverse[H]): Monad[({type f[x] = G[H[x]]})#f] = new Monad[({type f[x] = G[H[x]]})#f] {
//    def unit[A](a: => A): G[H[A]] = G.unit(H.unit(a))
//    override def flatMap[A,B](mna: G[H[A]])(f: A => G[H[B]]): G[H[B]] =
//      G.flatMap(mna)(na => G.map(T.traverse(na)(f))(H.join))
//  }
}

// Exercise 11.17
case class Id[A](value: A) {
  def map[B](f: A => B): Id[B] = Id(f(value))

  def flatMap[B](f: A => Id[B]): Id[B] = f(value)
}

// Exercise 11.20
case class Reader[R, A](run: R => A)

object Reader {
  def readerMonad[R] = new Monad[({type f[x] = Reader[R, x]})#f] {
    def unit[A](a: => A): Reader[R, A] = Reader(_ => a)
    // def flatMap[A, B](st: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] = Reader { r => f(st.run(r)).run(r) }
    override def flatMap[A, B](st: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] = Reader { st.run andThen f andThen { _.run } }
  }
}
case class OptionT[M[_],A](value: M[Option[A]])(implicit M: Monad[M]) {
  def flatMap[B](f: A => OptionT[M, B]): OptionT[M, B] =
    OptionT(M.flatMap(value) {
      case None => M.unit(None)
      case Some(a) => f(a).value
    })
}