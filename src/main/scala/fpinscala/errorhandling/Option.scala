package fpinscala.errorhandling

sealed trait Option[+A] {
  def map[B](f: A => B): Option[B] = this match {
    case Some(a) => Some(f(a))
    case None => None
  }
  def getOrElse[B >: A](default: => B): B = this match {
    case Some(a) => a
    case None => default
  }
  def flatMap[B](f:A => Option[B]): Option[B] = map(f) getOrElse None
  def orElse[B >: A](ob: => Option[B]): Option[B] = this match {
    case Some(_) => this
    case None => ob
  }
  def filter(f: A => Boolean): Option[A] = flatMap(a => if(f(a)) Some(a) else None)
}
case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]

object Option {
  def apply[A](a: A): Option[A] = a match {
    case null => None
    case _ => Some(a)
  }
  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  // Exercise 4.2
  def variance(xs: Seq[Double]): Option[Double] = mean(xs) flatMap (m => mean(xs.map(x => Math.pow(x - m, 2))))

  // Exercise 4.3
  def map2[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = for {
    va <- a
    vb <- b
  } yield f(va, vb)
  def map2_basic[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = a.flatMap(aa => b.map(bb => f(aa,bb)))

  // Exercise 4.4
  def sequence[A](a: List[Option[A]]): Option[List[A]] = a.foldRight[Option[List[A]]](Some(List()))((op, acc) => map2(op, acc)(_ :: _))

  // Exercise 4.5
  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] =
    a.foldRight[Option[List[B]]](Some(Nil))((op, acc) => map2(f(op), acc)(_ :: _))

  def sequence_1[A](a: List[Option[A]]): Option[List[A]] = traverse(a)(x => x)

  def apply_withMap2[A, B](fa: Option[A])(f: Option[A => B]): Option[B] = map2(fa, f)((a, f) => f(a))
  def apply[A, B](fa: Option[A])(f: Option[A => B]): Option[B] = fa.flatMap(a => f.map(_(a)))

  // Todo
  // def map2_withApply[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = apply(a)(b.map(bb => Some(f(aa, bb))))
}

object Text extends App {

  val n: Option[Int] = None
  val oi = Some(1)

  println(n.flatMap(i => Some(i.toString)))
  println(oi.flatMap(i => Some(s"Int: ${i.toString}")))
}